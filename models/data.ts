export interface SCHEDULE {
  time: string;
  sj_code: string;
  subject: string;
  lecturers: string;
  period: number[];
  floor: number;
  tower: string;
}
interface deviceType {}
export interface CLASS_ROOM {
  id: string | number;
  tower: string;
  classroom: string;
  student_name: string;
  student_code: string | number;
  sj_code: string;
  lecturers: string;
  subject: string;
  status?: number;
  device?: deviceType;
}
export let deviceType: Array<"micro" | "máy chiếu">;
export interface statusType {
  [key: number]: string;
}
