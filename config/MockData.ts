import { CLASS_ROOM, SCHEDULE, statusType } from "../models/data";

export const SCHEDULE_DATA: SCHEDULE[] = [
  {
    time: "2022-12-18T08:20:04Z",
    sj_code: "COMP411",
    subject: "Đại số TT",
    lecturers: "Nguyễn Thị Thuỷ",
    period: [1, 5],
    floor: 403,
    tower: "C",
  },
  {
    time: "2022-12-18T08:20:04Z",
    sj_code: "COMP411",
    subject: "Đại số TT",
    lecturers: "Nguyễn Thị Thuỷ",
    period: [1, 5],
    floor: 403,
    tower: "C",
  },
  {
    time: "2022-12-18T08:20:04Z",
    sj_code: "COMP411",
    subject: "Đại số TT",
    lecturers: "Nguyễn Thị Thuỷ",
    period: [1, 5],
    floor: 403,
    tower: "C",
  },
  {
    time: "2022-12-18T08:20:04Z",
    sj_code: "COMP411",
    subject: "Đại số TT",
    lecturers: "Nguyễn Thị Thuỷ",
    period: [1, 5],
    floor: 403,
    tower: "C",
  },
  {
    time: "2022-12-18T08:20:04Z",
    sj_code: "COMP411",
    subject: "Đại số TT",
    lecturers: "Nguyễn Thị Thuỷ",
    period: [1, 5],
    floor: 403,
    tower: "C",
  },
  {
    time: "2022-12-18T08:20:04Z",
    sj_code: "COMP411",
    subject: "Đại số TT",
    lecturers: "Nguyễn Thị Thuỷ",
    period: [1, 5],
    floor: 403,
    tower: "C",
  },
  {
    time: "2022-12-18T08:20:04Z",
    sj_code: "COMP411",
    subject: "Đại số TT",
    lecturers: "Nguyễn Thị Thuỷ",
    period: [1, 5],
    floor: 403,
    tower: "C",
  },
  {
    time: "2022-12-18T08:20:04Z",
    sj_code: "COMP411",
    subject: "Đại số TT",
    lecturers: "Nguyễn Thị Thuỷ",
    period: [1, 5],
    floor: 403,
    tower: "C",
  },
];

export const statusDetail: statusType = {
  0: "",
  1: "Đang sử dụng",
  2: "Đang sửa chữa",
  3: "Chưa duyệt",
};

export const CLASSROOM_DATA: CLASS_ROOM[] = [
  {
    id: "C1",
    tower: "C",
    classroom: "403",
    student_name: "Trần C",
    student_code: "685000000",
    sj_code: "COMP101",
    subject: "Lập trình C, C++",
    lecturers: "Trần Văn A",
    status: 1,
    device: ["micro", "máy chiếu"],
  },
  {
    id: "C2",
    tower: "C",
    student_name: "",
    student_code: "",
    classroom: "",
    sj_code: "",
    subject: "",
    lecturers: "",
    device: [],
  },
  {
    id: "B1",
    tower: "B",
    classroom: "202",
    student_name: "Trần C",
    student_code: "685000000",
    sj_code: "ABC101",
    subject: "Tâm lý học",
    lecturers: "Nguyễn Thị H",
    status: 1,
    device: ["micro"],
  },
  {
    id: "B2",
    tower: "B",
    classroom: "",
    student_name: "",
    student_code: "",
    sj_code: "",
    subject: "",
    lecturers: "",
    device: [],
    status: 2,
  },
  {
    id: "D1",
    tower: "D",
    classroom: "",
    student_name: "",
    student_code: "",
    sj_code: "",
    subject: "",
    lecturers: "",
    device: [],
  },
  {
    id: "D2",
    tower: "D",
    classroom: "",
    student_name: "",
    student_code: "",
    sj_code: "",
    subject: "",
    lecturers: "",
    device: [],
  },
];

export const CLASSROOM_DATA_2: CLASS_ROOM[] = [
  {
    id: "C1",
    tower: "C",
    classroom: "403",
    student_name: "Trần C",
    student_code: "685000000",
    sj_code: "COMP101",
    subject: "Lập trình C, C++",
    lecturers: "Trần Văn A",
    status: 3,
    device: ["micro", "máy chiếu", "Cây máy tính", "Bàn phím"],
  },
  {
    id: "C2",
    tower: "C",
    student_name: "Trần D",
    student_code: "685000005",
    classroom: "203",
    sj_code: "AOMP103",
    subject: "Xử lý ảnh",
    lecturers: "Nguyễn H",
    device: ["micro", "máy chiếu"],
    status: 3,
  },
  {
    id: "B1",
    tower: "B",
    classroom: "202",
    student_name: "Trần H",
    student_code: "685000000",
    sj_code: "ABC101",
    subject: "Tâm lý học",
    lecturers: "Nguyễn Thị H",
    status: 3,
    device: ["micro"],
  },
];

export const TowerData = [
  {
    value: "A",
    label: "A",
  },
  {
    value: "B",
    label: "B",
  },
  {
    value: "C",
    label: "C",
  },
  {
    value: "D",
    label: "D",
  },
];

export const floorList = [
  {
    value: "1",
    label: "1",
  },
  {
    value: "2",
    label: "2",
  },
  {
    value: "3",
    label: "3",
  },
  {
    value: "4",
    label: "4",
  },
];

export const TOWER_INFO = [
  {
    name: "B",
    room: [
      {
        id: "B1",
        room_name: "101",
        status: 0,
      },
      {
        id: "B2",
        room_name: "102",
        status: 1,
      },
      {
        id: "B3",
        room_name: "103",
        status: 0,
      },
      {
        id: "B4",
        room_name: "104",
        status: 2,
      },
    ],
  },
  {
    name: "C",
    room: [
      {
        id: "C1",
        room_name: "201",
        status: 0,
      },
      {
        id: "C2",
        room_name: "202",
        status: 0,
      },
      {
        id: "C3",
        room_name: "203",
        status: 1,
      },
      {
        id: "A4",
        room_name: "204",
        status: 1,
      },
    ],
  },
  {
    name: "A",
    room: [
      {
        id: "A1",
        room_name: "301",
        status: 2,
      },
      {
        id: "A2",
        room_name: "302",
        status: 1,
      },
      {
        id: "A3",
        room_name: "303",
        status: 0,
      },
      {
        id: "A4",
        room_name: "304",
        status: 1,
      },
    ],
  },
];

export const CLASSROOM_MANAGE = [
  {
    id: 1,
    tower: "A",
    floor: 1,
    class_name: 201,
    slot: 40,
    image:
      "https://images.unsplash.com/photo-1580582932707-520aed937b7b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=100",
  },
  {
    id: 2,
    tower: "B",
    floor: 1,
    class_name: 301,
    slot: 50,
    image:
      "https://images.unsplash.com/photo-1585637071663-799845ad5212?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=100",
  },
  {
    id: 3,
    tower: "C",
    floor: 1,
    class_name: 201,
    slot: 60,
    image:
      "https://images.unsplash.com/photo-1580582932707-520aed937b7b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=100",
  },
  {
    id: 4,
    tower: "D",
    floor: 1,
    class_name: 401,
    slot: 30,
    image:
      "https://images.unsplash.com/photo-1585637071663-799845ad5212?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=100",
  },
  {
    id: 5,
    tower: "E",
    floor: 1,
    class_name: 301,
    slot: 42,
    image:
      "https://images.unsplash.com/photo-1635424239131-32dc44986b56?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=100",
  },
  {
    id: 6,
    tower: "A",
    floor: 1,
    class_name: 203,
    slot: 200,
    image:
      "https://images.unsplash.com/photo-1541829070764-84a7d30dd3f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&q=100",
  },
  {
    id: 7,
    tower: "C",
    floor: 1,
    class_name: 502,
    slot: 40,
    image:
      "https://images.unsplash.com/photo-1635424239131-32dc44986b56?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=100",
  },
  {
    id: 8,
    tower: "C",
    floor: 1,
    class_name: 204,
    slot: 160,
    image:
      "https://images.unsplash.com/photo-1606761568499-6d2451b23c66?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&q=100",
  },
];

export const DEVICE_MANAGE = [
  {
    id: 1,
    device_name: "Micro",
    total: 1200,
    image:
      "https://images.unsplash.com/photo-1615661433945-8c7ee7e7cef1?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=120&h=120",
  },
  {
    id: 2,
    device_name: "Máy chiếu",
    total: 1200,
    image:
      "https://images.unsplash.com/photo-1528395874238-34ebe249b3f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=120&h=120",
  },
  {
    id: 2,
    device_name: "Điều hoà",
    total: 2400,
    image:
      "https://images.unsplash.com/photo-1590756254933-2873d72a83b6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=120&h=120",
  },
  {
    id: 3,
    device_name: "Quạt trần",
    total: 4000,
    image:
      "https://images.unsplash.com/photo-1581153691064-8d0ec09725b9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=120&h=120",
  },
  {
    id: 4,
    device_name: "Cây máy tính",
    total: 1000,
    image:
      "https://plus.unsplash.com/premium_photo-1671439429636-6d8d66247143?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=120&h=120",
  },
  {
    id: 5,
    device_name: "Bàn phím",
    total: 1000,
    image:
      "https://images.unsplash.com/photo-1587829741301-dc798b83add3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=120&h=120",
  },
  {
    id: 6,
    device_name: "Màn hình máy tính",
    total: 1000,
    image:
      "https://images.unsplash.com/photo-1527443195645-1133f7f28990?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=120&h=120",
  },
];
