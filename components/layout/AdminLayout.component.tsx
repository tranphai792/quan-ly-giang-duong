import React, { useEffect, useState } from "react";
import { MENU_TYPE } from "../../models/menu";
import { PicRightOutlined } from "@ant-design/icons";
import Link from "next/link";
import MainLayout from "./MainLayout.component";
export interface AdminLayoutProps {
  children: React.ReactNode;
}

export default function AdminLayout({ children }: AdminLayoutProps) {
  const [title, setTitle] = useState("");
  const [role, setRole] = useState();
  useEffect(() => {
    // Perform localStorage action
    const user = JSON.parse(localStorage.getItem("user") || "{}");
    setRole(user.role);
  }, []);
  const menu_user: MENU_TYPE[] = [
    {
      key: "/",
      icon: <PicRightOutlined />,
      label: (
        <Link href="/" onClick={() => setTitle("Lịch học")}>
          Lịch
        </Link>
      ),
    },
    {
      key: "/class-list",
      icon: <PicRightOutlined />,

      label: (
        <Link href="/class-list" onClick={() => setTitle("Phòng học")}>
          Phòng học
        </Link>
      ),
    },
    {
      key: "/class-use",
      icon: <PicRightOutlined />,

      label: (
        <Link href="/class-use" onClick={() => setTitle("Phòng đã đăng ký")}>
          Phòng đã đăng ký
        </Link>
      ),
    },
  ];

  const menu_admin: MENU_TYPE[] = [
    {
      key: "/class-manage",
      icon: <PicRightOutlined />,
      label: (
        <Link
          href="/class-manage"
          onClick={() => setTitle(" Quản lý phòng học")}
        >
          Quản lý phòng học
        </Link>
      ),
    },
    {
      key: "/approval-class",
      icon: <PicRightOutlined />,
      label: (
        <Link
          href="/approval-class"
          onClick={() => setTitle(" Quản lý phòng học")}
        >
          Duyệt yêu cầu mượn phòng
        </Link>
      ),
    },

    {
      key: "/device-manage",
      icon: <PicRightOutlined />,

      label: (
        <Link
          href="/device-manage"
          onClick={() => setTitle("Quản lý thiết bị")}
        >
          Quản lý thiết bị
        </Link>
      ),
    },
    {
      key: "/chart",
      icon: <PicRightOutlined />,

      label: (
        <Link href="/chart" onClick={() => setTitle("Biểu đồ")}>
          Biểu đồ
        </Link>
      ),
    },
  ];

  return (
    <MainLayout menu={role == 0 ? menu_admin : menu_user} pageName={title}>
      {children}
    </MainLayout>
  );
}
