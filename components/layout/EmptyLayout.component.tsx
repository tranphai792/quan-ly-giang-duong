import * as React from "react";
export interface EmptyLayoutProps {
  children: React.ReactNode;
}
const EmptyLayout = ({ children }: EmptyLayoutProps) => {
  return <div>{children}</div>;
};
export default EmptyLayout;
