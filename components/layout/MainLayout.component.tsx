import React, { useEffect, useState } from "react";
import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import { Layout, Menu, theme } from "antd";
import { Input } from "antd";
import styles from "./MainLayout.module.scss";
import { useRouter } from "next/router";
const { Header, Sider, Content, Footer } = Layout;
const { Search } = Input;
type MainLayoutProps = {
  menu?: any;
  pageName?: string;
  children: React.ReactNode;
};

const MainLayout = ({ menu, pageName, children }: MainLayoutProps) => {
  const route = useRouter();
  const [role, setRole] = useState();
  const [pathName, setPathName] = useState(route.pathname);
  const [collapsed, setCollapsed] = useState(false);
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user") || "{}");
    setRole(user.role);
    if (!user.username) {
      route.push("/login");
    }
  });
  const handleLogout = () => {
    localStorage.removeItem("user");
    route.push("/login");
  };
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className={styles.logo}>
          <h1>{role === 0 ? "ADMIN" : "USER"}</h1>
        </div>
        <Menu mode="inline" defaultSelectedKeys={[pathName]} items={menu} />
      </Sider>
      <Layout
        className="site-layout"
        style={{ background: "rgb(246,249,255)" }}
      >
        <Header
          style={{
            padding: 0,
            background: "white",
            display: "flex",
            alignItems: "center",
          }}
        >
          <div
            className={styles.trigger}
            onClick={() => setCollapsed(!collapsed)}
          >
            {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
          </div>
          <div
            style={{
              padding: 0,
              display: "flex",
              alignItems: "center",
              flexGrow: 1,
              justifyContent: "center",
            }}
          >
            <Search placeholder="Tìm kiếm" style={{ width: 400 }} />
          </div>
          <p
            style={{
              marginRight: "20px",
              fontWeight: 600,
              fontSize: "18px",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={handleLogout}
          >
            Đăng xuất
          </p>
        </Header>
        <h1 className={styles.MainContent__header}>{pageName}</h1>
        <Content className={styles.MainContent__content}>{children}</Content>
        <Footer className={styles.MainContent__footer}>
          Phòng CNTT Đại Học Sư Phạm Hà Nội
        </Footer>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
