import { Input, Table, Tag } from "antd";
import type { ColumnsType } from "antd/es/table";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { statusDetail } from "../../config/MockData";
import { CLASS_ROOM } from "../../models/data";
import { removeClassRoom } from "../../redux/slices/classrooms";
import { RootState } from "../../redux/store";
import styles from "./ClassUsed.module.scss";

export interface ClassRoomUsedProps {}

const { Search } = Input;
export default function ClassRoomUsed(props: ClassRoomUsedProps) {
  const dataClass = useSelector(
    (state: RootState) => state.classUsed.classRoom
  );
  const dispatch = useDispatch();
  const handleRemoveClass = (id: any) => {
    dispatch(removeClassRoom(id));
  };
  const columns: ColumnsType<CLASS_ROOM> = [
    {
      title: "id",
      dataIndex: "id",
      key: "id",
      render: (text) => <div className={styles.time}> {text}</div>,
    },
    {
      title: "Toà nhà",
      dataIndex: "tower",
      key: "tower",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Số phòng",
      dataIndex: "classroom",
      key: "classroom",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Sinh viên ĐK",
      dataIndex: "student_name",
      key: "student_name",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Mã sinh viên",
      dataIndex: "student_code",
      key: "student_code",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Mã môn",
      dataIndex: "sj_code",
      key: "sj_code",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Tên môn học",
      dataIndex: "subject",
      key: "subject",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Giảng viên",
      dataIndex: "lecturers",
      key: "lecturers",
      render: (text) => <div className={styles.lecturers}>{text}</div>,
    },
    {
      title: "Thiết bị đã mượn",
      dataIndex: "device",
      key: "device",
      render: (arrPeriod) => <div>{arrPeriod?.join(",")}</div>,
    },
    {
      title: "Tình trạng",
      dataIndex: "status",
      key: "status",
      render: (status) => (
        <Tag color={status === 1 ? "green" : status == 2 ? "volcano" : "white"}>
          {statusDetail[status]}
        </Tag>
      ),
    },

    {
      title: "Thao tác",
      key: "action",
      render: (_, { id }) => (
        <button
          style={{
            background: "#FF4500",
            border: "none",
            padding: "10px",
            borderRadius: "10px",
            color: "white",
            cursor: "pointer",
          }}
          onClick={() => handleRemoveClass(id)}
        >
          Trả phòng
        </button>
      ),
    },
  ];

  return (
    <section className={styles.class_room}>
      <div className={styles.class_room__header}>
        <h2 className={styles.class_room__header__title}>
          Danh sách phòng học
        </h2>
      </div>
      <div className={styles.class_room__table}>
        <Table
          rowKey={(row) => row.id}
          dataSource={dataClass}
          columns={columns}
          pagination={{
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} trong tổng số ${total} mục`,
            pageSize: 7,
          }}
        />
      </div>
    </section>
  );
}
