import * as React from "react";
import { DatePicker, Input, Table, Tag } from "antd";
import type { ColumnsType } from "antd/es/table";
import { SCHEDULE } from "../../models/data";
import { EditTwoTone, DeleteFilled, EyeFilled } from "@ant-design/icons";
import { SCHEDULE_DATA } from "../../config/MockData";
import moment from "moment";
import styles from "./Schedule.module.scss";
export interface AdminProps {}
const { RangePicker } = DatePicker;
const { Search } = Input;
export default function AdminPage(props: AdminProps) {
  const columns: ColumnsType<SCHEDULE> = [
    {
      title: "Thời gian",
      dataIndex: "time",
      key: "time",
      render: (text) => (
        <div className={styles.time}> {moment(text).format("DD-MM-YYYY")}</div>
      ),
    },
    {
      title: "Mã môn",
      dataIndex: "sj_code",
      key: "sj_code",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Tên môn",
      dataIndex: "subject",
      key: "subject",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Giảng viên",
      dataIndex: "lecturers",
      key: "lecturers",
      render: (text) => <div className={styles.lecturers}>{text}</div>,
    },
    {
      title: "Tiết",
      dataIndex: "period",
      key: "period",
      render: (arrPeriod) => <div>{arrPeriod.join("-")}</div>,
    },
    {
      title: "Phòng",
      key: "classroom",
      render: (_, { floor, tower }) => (
        <Tag color="geekblue">{`${floor}-${tower}`.toUpperCase()}</Tag>
      ),
    },
    {
      title: "Thao tác",
      key: "action",
      render: () => (
        <div>
          <span className={styles.action}>
            <EditTwoTone />
          </span>
          <span className={styles.action}>
            <DeleteFilled />
          </span>
          <span className={styles.action}>
            <EyeFilled />
          </span>
        </div>
      ),
    },
  ];
  return (
    <section className={styles.admin__main}>
      <div className={styles.admin__main__header}>
        <h2 className={styles.admin__main__header__title}>Thời khoá biểu</h2>
        <div className={styles.admin__main__header__tools}>
          <RangePicker />
          <Search placeholder="Tìm kiếm" style={{ width: 300 }} />
        </div>
      </div>
      <div className={styles.admin__main__table}>
        <Table
          dataSource={SCHEDULE_DATA}
          columns={columns}
          pagination={{
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} trong tổng số ${total} mục`,
            pageSize: 7,
          }}
        />
      </div>
    </section>
  );
}
