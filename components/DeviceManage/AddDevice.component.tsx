import { Button, Form, Input, InputNumber } from "antd";
import { forwardRef, useEffect } from "react";
import ModalAntd from "../common/ModalAntd/ModalAntd";
interface AddClassProps {}

const AddDevice = (props: AddClassProps, ref: any) => {
  const handleAdd = () => {
    ref.current.handleCancel();
  };
  const handleCancel = () => {
    ref.current.handleCancel();
  };
  return (
    <ModalAntd ref={ref} title="Thêm thiết bị">
      <Form
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        initialValues={{ remember: true }}
        onFinish={handleAdd}
        //   onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <div style={{ display: "flex", gap: "20px" }}>
          <Form.Item
            label="Tên thiết bị"
            name="tower"
            rules={[{ required: true, message: "Nhập tên thiết bị!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Số lượng"
            name="total"
            rules={[{ required: true, message: "Nhập số tầng" }]}
          >
            <InputNumber />
          </Form.Item>
        </div>

        <Form.Item label="Ảnh" name="image">
          <input type="file" />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 24 }} style={{ marginTop: "20px" }}>
          <div
            style={{ display: "flex", justifyContent: "center", gap: "20px" }}
          >
            <Button type="primary" htmlType="submit">
              Thêm thiết bị
            </Button>
            <Button type="default" onClick={handleCancel}>
              Huỷ
            </Button>
          </div>
        </Form.Item>
      </Form>
    </ModalAntd>
  );
};

export default forwardRef(AddDevice);
