import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import clsx from "clsx";
import { useRef } from "react";
import { CLASSROOM_MANAGE, DEVICE_MANAGE } from "../../config/MockData";
import AddClass from "./AddDevice.component";
import styles from "./DeviceManage.module.scss";
type Props = {};
const DeviceManage = (props: Props) => {
  const refEdit = useRef<any>(null);
  const handleAdd = () => {
    refEdit.current.showModal();
  };
  const handleEdit = () => {
    refEdit.current.showModal();
  };
  return (
    <section className={styles.device_manage}>
      <button
        className={clsx(
          styles.device_manage__button,
          styles.device_manage__button__add
        )}
        onClick={handleAdd}
      >
        <PlusOutlined />
        Thêm
      </button>
      <div className={styles.device_manage__main}>
        {DEVICE_MANAGE.map((item) => (
          <div className={styles.device_manage__container} key={item.id}>
            <div className={styles.device_manage__left}>
              <div className={styles.device_manage__left__container}>
                <div className={styles.device_manage__item}>
                  <span className={styles.device_manage__item__title}>
                    Mã thiết bị:
                  </span>
                  <span>{item.id}</span>
                </div>
                <div className={styles.device_manage__item}>
                  <span className={styles.device_manage__item__title}>
                    Tên thiết bị:
                  </span>
                  <span>{item.device_name}</span>
                </div>
              </div>

              <div className={styles.device_manage__item}>
                <span className={styles.device_manage__item__title}>
                  Số lượng:
                </span>
                <span>{item.total}</span>
              </div>
            </div>
            <div className={styles.device_manage__right}>
              <p className={styles.device_manage__item__title}>Hình ảnh:</p>
              <img src={item.image} alt="anh" />
            </div>
            <div className={styles.device_manage__action}>
              <button
                className={clsx(
                  styles.device_manage__button,
                  styles.device_manage__button__edit
                )}
                onClick={handleEdit}
              >
                <EditOutlined />
                Sửa
              </button>

              <button
                className={clsx(
                  styles.device_manage__button,
                  styles.device_manage__button__delete
                )}
              >
                <DeleteOutlined />
                Xoá
              </button>
            </div>
          </div>
        ))}
      </div>
      <AddClass ref={refEdit} />
    </section>
  );
};
export default DeviceManage;
