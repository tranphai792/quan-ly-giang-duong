import React from "react";
import { Button, Checkbox, Form, Input, notification } from "antd";
import styles from "./LoginPage.module.scss";
import { useRouter } from "next/router";
type LoginPageProps = {};

const ListAcc = [
  {
    username: "admin",
    password: "admin",
    role: 0,
  },
  {
    username: "user",
    password: "123456",
    role: 1,
  },
];

const onFinishFailed = () => {};

export const LoginPage = (props: LoginPageProps) => {
  const route = useRouter();

  const onFinish = (value: any) => {
    const checkPass = ListAcc.some(
      (acc) =>
        acc.username === value.username && acc.password === value.password
    );
    const indexAcc = ListAcc.findIndex(
      (acc) => acc.username === value.username
    );
    if (checkPass) {
      localStorage.setItem("user", JSON.stringify(ListAcc[indexAcc]));
      if (ListAcc[indexAcc].role === 1) {
        route.push("/");
      } else {
        route.push("/class-manage");
      }
    } else {
      notification.error({
        message: "Sai tên đăng nhập hoặc mật khẩu",
      });
    }
  };

  return (
    <section className={styles.LoginPage}>
      <div className={styles.LoginPage__container}>
        <h2 className={styles.LoginPage__title}>
          Hệ thống quản lý giảng đường
        </h2>
        <div className={styles.LoginPageForm}>
          <div className={styles.LoginPageForm__header}>
            <p className={styles.LoginPageForm__header__title}>Đăng nhập</p>
            <p>Nhập tài khoản và mật khẩu</p>
          </div>
          <div className={styles.LoginPageForm__main}>
            <Form
              name="basic"
              labelCol={{ span: 24 }}
              wrapperCol={{ span: 24 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Tài khoản"
                name="username"
                rules={[
                  { required: true, message: "Vui lòng điền tên tài khoản!" },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Mật khẩu"
                name="password"
                rules={[{ required: true, message: "Vui lòng điền mật khẩu" }]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                name="remember"
                valuePropName="checked"
                wrapperCol={{ offset: 8, span: 16 }}
              >
                <Checkbox>Remember me</Checkbox>
              </Form.Item>

              <Form.Item
                wrapperCol={{ offset: 8, span: 16 }}
                className={styles.LoginPageForm__submit}
              >
                <Button
                  type="primary"
                  htmlType="submit"
                  className={styles.LoginPageForm__submit__button}
                >
                  Đăng nhập
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </section>
  );
};
