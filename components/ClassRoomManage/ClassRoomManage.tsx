import React, { useEffect, useRef, useState } from "react";
import styles from "./ClassRoomManage.module.scss";
type Props = {};
import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { CLASSROOM_MANAGE, TowerData } from "../../config/MockData";
import clsx from "clsx";
import ModalAntd from "../common/ModalAntd/ModalAntd";
import AddClass from "./AddClass.component";
import { Select } from "antd";
const ClassRoomManage = (props: Props) => {
  const [dataClass, setDataClass] = useState(CLASSROOM_MANAGE);
  const [tower, setTower] = useState("Toà nhà");
  const refEdit = useRef<any>(null);
  const handleAdd = () => {
    refEdit.current.showModal();
  };
  const handleEdit = () => {
    refEdit.current.showModal();
  };

  const handleChangeTower = (value: any) => {
    const dataFillter = CLASSROOM_MANAGE.filter((data) => {
      return data.tower === value;
    });
    setDataClass(dataFillter);
    setTower(value);
  };

  const handleResset = () => {
    setDataClass(CLASSROOM_MANAGE);
  };
  return (
    <section className={styles.class_manage}>
      <button
        className={clsx(
          styles.class_manage__button,
          styles.class_manage__button__add
        )}
        onClick={handleAdd}
      >
        <PlusOutlined />
        Thêm
      </button>
      <Select
        defaultValue={tower}
        style={{ width: 120, marginLeft: "20px" }}
        onChange={handleChangeTower}
        options={TowerData}
        value={tower}
      />
      <button
        style={{ width: 120, marginLeft: "20px" }}
        className={clsx(
          styles.class_manage__button,
          styles.class_manage__button__edit
        )}
        onClick={handleResset}
      >
        Resset
      </button>
      <div className={styles.class_manage__main}>
        {dataClass.map((item) => (
          <div className={styles.class_manage__container} key={item.id}>
            <div className={styles.class_manage__left}>
              <div className={styles.class_manage__left__container}>
                <div className={styles.class_manage__item}>
                  <span className={styles.class_manage__item__title}>
                    Tòa nhà:
                  </span>{" "}
                  <span>{item.tower}</span>
                </div>
                <div className={styles.class_manage__item}>
                  <span className={styles.class_manage__item__title}>
                    Tầng:
                  </span>
                  <span>{item.floor}</span>
                </div>
              </div>
              <div className={styles.class_manage__left__container}>
                <div className={styles.class_manage__item}>
                  <span className={styles.class_manage__item__title}>
                    Số phòng:
                  </span>
                  <span>{item.class_name}</span>
                </div>
                <div className={styles.class_manage__item}>
                  <span className={styles.class_manage__item__title}>
                    Số ghế:
                  </span>
                  <span>{item.slot}</span>
                </div>
              </div>
            </div>
            <div className={styles.class_manage__right}>
              <p className={styles.class_manage__item__title}>Hình ảnh:</p>
              <img src={item.image} alt="anh" />
            </div>
            <div className={styles.class_manage__action}>
              <button
                className={clsx(
                  styles.class_manage__button,
                  styles.class_manage__button__edit
                )}
                onClick={handleEdit}
              >
                <EditOutlined />
                Sửa
              </button>

              <button
                className={clsx(
                  styles.class_manage__button,
                  styles.class_manage__button__delete
                )}
              >
                <DeleteOutlined />
                Xoá
              </button>
            </div>
          </div>
        ))}
      </div>
      <AddClass ref={refEdit} />
    </section>
  );
};
export default ClassRoomManage;
