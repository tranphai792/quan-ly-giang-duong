import { Button, Form, Input, Select } from "antd";
import React, { forwardRef, useEffect } from "react";
import { TowerData } from "../../config/MockData";
import ModalAntd from "../common/ModalAntd/ModalAntd";
import { InputNumber } from "antd";
interface AddClassProps {}

const AddClass = (props: AddClassProps, ref: any) => {
  useEffect(() => {
    console.log(ref.current);
  });
  const handleAdd = () => {
    ref.current.handleCancel();
  };
  const handleCancel = () => {
    ref.current.handleCancel();
  };
  return (
    <ModalAntd ref={ref} title="Thêm phòng học">
      <Form
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        initialValues={{ remember: true }}
        onFinish={handleAdd}
        //   onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <div style={{ display: "flex", gap: "20px" }}>
          <Form.Item
            label="Toà nhà"
            name="tower"
            rules={[{ required: true, message: "Nhập tên toà nhà!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Tầng"
            name="floor"
            rules={[{ required: true, message: "Nhập số tầng" }]}
          >
            <InputNumber />
          </Form.Item>
        </div>
        <div style={{ display: "flex", gap: "20px" }}>
          <Form.Item
            label="Số phòng"
            name="classname"
            rules={[{ required: true, message: "Nhập số phòng" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Số ghế"
            name="slot"
            rules={[{ required: true, message: "Nhập sức chứa" }]}
          >
            <InputNumber />
          </Form.Item>
        </div>
        <Form.Item style={{ marginTop: "20px" }} label="Ảnh" name="image">
          <input type="file" />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 24 }} style={{ marginTop: "20px" }}>
          <div
            style={{ display: "flex", justifyContent: "center", gap: "20px" }}
          >
            <Button type="primary" htmlType="submit">
              Thêm phòng học
            </Button>
            <Button type="default" onClick={handleCancel}>
              Huỷ
            </Button>
          </div>
        </Form.Item>
      </Form>
    </ModalAntd>
  );
};

export default forwardRef(AddClass);
