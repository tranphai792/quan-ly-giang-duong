import { DatePicker, Input, Table, Tag } from "antd";
import React from "react";
import type { ColumnsType } from "antd/es/table";

import {
  CLASSROOM_DATA_2,
  SCHEDULE_DATA,
  statusDetail,
} from "../../config/MockData";
import { CLASS_ROOM } from "../../models/data";
import styles from "./ApprovalClass.module.scss";
type Props = {};
const { RangePicker } = DatePicker;
const { Search } = Input;

const ApprovalClass = (props: Props) => {
  const columns: ColumnsType<CLASS_ROOM> = [
    {
      title: "id",
      dataIndex: "id",
      key: "id",
      render: (text) => <div className={styles.time}> {text}</div>,
    },
    {
      title: "Toà nhà",
      dataIndex: "tower",
      key: "tower",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Số phòng",
      dataIndex: "classroom",
      key: "classroom",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Sinh viên ĐK",
      dataIndex: "student_name",
      key: "student_name",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Mã sinh viên",
      dataIndex: "student_code",
      key: "student_code",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Mã môn",
      dataIndex: "sj_code",
      key: "sj_code",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Tên môn học",
      dataIndex: "subject",
      key: "subject",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Giảng viên",
      dataIndex: "lecturers",
      key: "lecturers",
      render: (text) => <div className={styles.lecturers}>{text}</div>,
    },
    {
      title: "Thiết bị muốn mượn",
      dataIndex: "device",
      key: "device",
      render: (arrPeriod) => <div>{arrPeriod?.join(",")}</div>,
    },
    {
      title: "Tình trạng",
      dataIndex: "status",
      key: "status",
      render: (status) => (
        <Tag
          color={
            status === 1
              ? "green"
              : status == 2
              ? "volcano"
              : status == 3
              ? "blue"
              : "white"
          }
        >
          {statusDetail[status]}
        </Tag>
      ),
    },

    {
      title: "Thao tác",
      key: "action",
      render: (_, { id }) => (
        <button
          style={{
            background: "blue",
            border: "none",
            padding: "10px",
            borderRadius: "10px",
            color: "white",
            cursor: "pointer",
          }}
          //   onClick={() => handleRemoveClass(id)}
        >
          Xác nhận
        </button>
      ),
    },
  ];
  return (
    <section className={styles.admin__main}>
      <div className={styles.admin__main__header}>
        <h2 className={styles.admin__main__header__title}>Duyệt yêu cầu</h2>
        <div className={styles.admin__main__header__tools}>
          <RangePicker />
          <Search placeholder="Tìm kiếm" style={{ width: 300 }} />
        </div>
      </div>
      <div className={styles.admin__main__table}>
        <Table
          dataSource={CLASSROOM_DATA_2}
          columns={columns}
          pagination={{
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} trong tổng số ${total} mục`,
            pageSize: 7,
          }}
        />
      </div>
    </section>
  );
};

export default ApprovalClass;
