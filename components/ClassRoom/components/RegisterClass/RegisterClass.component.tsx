import React, { useEffect, useState } from "react";
import { Button, Modal, Form, Input, Select } from "antd";
import { TOWER_INFO } from "../../../../config/MockData";
import { deviceType } from "../../../../models/data";
interface RegisterClassProps {
  isModalOpen: boolean;
  handleOk: any;
  handleOpenModal: any;
}
interface optionSelect {
  value: any;
  label: string;
}

const RegisterClass = ({
  isModalOpen,
  handleOk,
  handleOpenModal,
}: RegisterClassProps) => {
  const [form] = Form.useForm();

  const [tower, setTower] = useState<optionSelect[]>([
    { label: "", value: "" },
  ]);

  const [towerChoose, setTowerChoose] = useState("");

  const [classroom, setClassroom] = useState<optionSelect[]>([
    { label: "", value: "" },
  ]);

  const handleSendForm = (value: any) => {
    handleOk(value);
    form.resetFields();
  };

  useEffect(() => {
    const listTower = TOWER_INFO.map((value) => {
      const item = {
        value: value.name,
        label: value.name,
      };
      return item;
    });
    setTower(listTower);
  }, []);

  useEffect(() => {
    const fillterClass =
      TOWER_INFO.find((item) => item.name === towerChoose)?.room || [];
    const listClass = fillterClass.map((value) => {
      const item = {
        value: `${value.room_name}-${value.id}`,
        label: `${value.room_name} - ${
          value.status === 1
            ? "Đang sử dụng"
            : value.status === 2
            ? "Đang sửa chữa"
            : "Trống"
        }`,
        disabled: value.status === 1 || value.status === 2,
      };
      return item;
    });
    setClassroom(listClass);
  }, [towerChoose]);
  return (
    <Modal
      title="Đăng ký phòng học"
      open={isModalOpen}
      onOk={handleSendForm}
      onCancel={handleOpenModal}
      centered={true}
      footer={null}
    >
      <Form
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        initialValues={{ remember: true }}
        onFinish={handleOk}
        //   onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Họ và tên"
          name="student_name"
          rules={[{ required: true, message: "Nhập họ và tên của bạn!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Mã sinh viên"
          name="student_code"
          rules={[{ required: true, message: "Xin mời nhập mã sinh viên" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Mã môn học"
          name="sj_code"
          rules={[{ required: true, message: "Xin mời nhập mã sinh viên" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Tên môn"
          name="subject"
          rules={[{ required: true, message: "Xin mời nhập mã sinh viên" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Giảng viên"
          name="lecturers"
          rules={[{ required: true, message: "Xin mời nhập mã sinh viên" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="Khoa" name="Khoa">
          <Input />
        </Form.Item>

        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Form.Item label="Toà nhà" name="tower">
            <Select
              style={{ width: 120 }}
              options={tower}
              onChange={(value: any) => setTowerChoose(value)}
            />
          </Form.Item>
          <Form.Item
            label="Phòng"
            name="classroom"
            rules={[{ required: true, message: "Xin mời chọn phòng học" }]}
          >
            <Select style={{ width: 200 }} options={classroom} />
          </Form.Item>
        </div>
        <Form.Item label="Thiết bị muốn mượn" name="device">
          <Select
            mode="multiple"
            placeholder="Please select"
            style={{ width: "100%" }}
            options={[
              {
                value: "micro",
                label: "micro",
              },
              {
                value: "Máy chiếu",
                label: "Máy chiếu",
              },
            ]}
          />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Đăng ký
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default RegisterClass;
