import { DeleteFilled, EditTwoTone, EyeFilled } from "@ant-design/icons";
import { Input, Table, Tag, Select, Button } from "antd";
import type { ColumnsType } from "antd/es/table";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  CLASSROOM_DATA,
  floorList,
  statusDetail,
  TowerData,
} from "../../config/MockData";
import { CLASS_ROOM } from "../../models/data";
import { addClassRoom } from "../../redux/slices/classrooms";
import styles from "./ClassRoom.module.scss";
import RegisterClass from "../ClassRoom/components/RegisterClass/RegisterClass.component";
import { useRouter } from "next/router";
export interface ClassRoomPageProps {}

const { Search } = Input;
export default function ClassRoomPage(props: ClassRoomPageProps) {
  const [tower, setTower] = useState("Toà nhà");
  const [dataClass, setDataClass] = useState(CLASSROOM_DATA);
  const [isOpenModal, setIsOpenModal] = useState(false);
  const route = useRouter();
  const dispatch = useDispatch();
  const columns: ColumnsType<CLASS_ROOM> = [
    {
      title: "id",
      dataIndex: "id",
      key: "id",
      render: (text) => <div className={styles.time}> {text}</div>,
    },
    {
      title: "Toà nhà",
      dataIndex: "tower",
      key: "tower",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Số phòng",
      dataIndex: "classroom",
      key: "classroom",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Sinh viên ĐK",
      dataIndex: "student_name",
      key: "student_name",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Mã sinh viên",
      dataIndex: "student_code",
      key: "student_code",
      render: (text) => <div>{text}</div>,
    },
    {
      title: "Mã môn",
      dataIndex: "sj_code",
      key: "sj_code",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Tên môn học",
      dataIndex: "subject",
      key: "subject",
      render: (text) => <div className={styles.sj_code}>{text}</div>,
    },
    {
      title: "Giảng viên",
      dataIndex: "lecturers",
      key: "lecturers",
      render: (text) => <div className={styles.lecturers}>{text}</div>,
    },
    {
      title: "Thiết bị đã mượn",
      dataIndex: "device",
      key: "device",
      render: (arrPeriod) => <div>{arrPeriod?.join(",")}</div>,
    },
    {
      title: "Tình trạng",
      dataIndex: "status",
      key: "status",
      render: (status) => (
        <Tag color={status === 1 ? "green" : status == 2 ? "volcano" : "white"}>
          {statusDetail[status]}
        </Tag>
      ),
    },

    {
      title: "Thao tác",
      key: "action",
      render: () => (
        <div>
          <span className={styles.action}>
            <EditTwoTone />
          </span>
          <span className={styles.action}>
            <DeleteFilled />
          </span>
          <span className={styles.action}>
            <EyeFilled />
          </span>
        </div>
      ),
    },
  ];
  const handleChangeTower = (value: any) => {
    const dataFillter = CLASSROOM_DATA.filter((data) => {
      return data.tower === value;
    });
    setDataClass(dataFillter);
    setTower(value);
  };
  const handleReset = () => {
    setTower("Toà nhà");
    setDataClass(CLASSROOM_DATA);
  };
  const handleRegistClassSucess = (value: any) => {
    const classroom = value.classroom.split("-")[0];
    const id = value.classroom.split("-")[1];
    const item = {
      ...value,
      id,
      classroom,
      status: 1,
    };

    dispatch(addClassRoom(item));
    setDataClass((prev) => [...prev, item]);
    setIsOpenModal(!isOpenModal);
    route.push("/class-use");
  };

  const handleRegistClass = () => {
    setIsOpenModal(!isOpenModal);
  };
  return (
    <section className={styles.class_room}>
      <div className={styles.class_room__header}>
        <h2 className={styles.class_room__header__title}>
          Danh sách phòng học
        </h2>
        <div className={styles.class_room__header__tools}>
          <div className={styles.class_room__header__tools__left}>
            <div className={styles.class_room__header__tools__element}>
              <Select
                defaultValue={tower}
                style={{ width: 120 }}
                onChange={handleChangeTower}
                options={TowerData}
                value={tower}
              />
            </div>
            <div className={styles.class_room__header__tools__element}>
              <Button type="primary" onClick={handleReset}>
                Reset bộ lọc
              </Button>
            </div>
          </div>
          <div>
            <Button type="primary" onClick={handleRegistClass}>
              Đăng ký lớp học
            </Button>
          </div>
        </div>
      </div>
      <div className={styles.class_room__table}>
        <Table
          rowKey={(row) => row.id}
          dataSource={dataClass}
          columns={columns}
          pagination={{
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} trong tổng số ${total} mục`,
            pageSize: 7,
          }}
        />
      </div>
      <RegisterClass
        isModalOpen={isOpenModal}
        handleOk={handleRegistClassSucess}
        handleOpenModal={handleRegistClass}
      />
    </section>
  );
}
