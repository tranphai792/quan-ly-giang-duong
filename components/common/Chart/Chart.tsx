import React from "react";
import { Bar } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
interface dataType {
  label: string;
  total: number;
  using: number;
  broken: number;
}
interface ChartProps {
  dataList: dataType[];
  title: string;
}
ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

const Chart = ({ dataList, title }: ChartProps) => {
  const options = {
    plugins: {
      legend: {
        position: "bottom" as const,
      },
      title: {
        display: true,
        text: title,
      },
    },
  };
  const data = {
    labels: dataList?.map((value) => value.label),
    datasets: [
      {
        label: "Đang sử dụng",
        data: dataList.map((item) => item.using),
        backgroundColor: "rgba(255, 99, 132, 0.5)",
      },
      {
        label: "Hỏng",
        data: dataList.map((item) => item.broken),
        backgroundColor: "rgba(53, 162, 235, 0.5)",
      },
    ],
  };
  return <Bar data={data} options={options} />;
};

export default Chart;
