import React, { forwardRef, useImperativeHandle, useState } from "react";
import { Button, Modal } from "antd";

interface Props {
  children: React.ReactNode;
  title: string;
}

const ModalAntd = ({ children, title }: Props, ref: any) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  useImperativeHandle(ref, () => ({
    showModal() {
      setIsModalOpen(true);
    },
    handleCancel() {
      setIsModalOpen(false);
    },
  }));
  return (
    <>
      <Modal
        title={title}
        open={isModalOpen}
        onCancel={() => {
          setIsModalOpen(false);
        }}
        footer={null}
        centered={true}
      >
        {children}
      </Modal>
    </>
  );
};

export default forwardRef(ModalAntd);
