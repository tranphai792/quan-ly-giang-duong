import React from "react";
import Chart from "../../../common/Chart/Chart";
import styles from "./ClassChart.module.scss";

const ClassChart = () => {
  const mockData = [
    {
      label: "A",
      using: 1000,
      broken: 1200 - 1000,
      total: 1200,
    },
    {
      label: "B",
      using: 1600,
      broken: 2000 - 1600,
      total: 2000,
    },
  ];
  return (
    <div className={styles.chart_component}>
      <Chart dataList={mockData} title="Biểu đồ lớp học" />
    </div>
  );
};

export default ClassChart;
