import React from "react";
import Chart from "../../../common/Chart/Chart";
import styles from "./DeviceChart.module.scss";
const DeviceChart = () => {
  const mockData = [
    {
      label: "Micro",
      using: 1000,
      broken: 1200 - 1000,
      total: 1200,
    },
    {
      label: "Máy chiếu",
      using: 1600,
      broken: 2000 - 1600,
      total: 2000,
    },
  ];
  return (
    <div className={styles.chart_components}>
      <Chart dataList={mockData} title="Biểu đồ thiết bị" />
    </div>
  );
};

export default DeviceChart;
