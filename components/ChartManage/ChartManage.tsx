import React from "react";
import { Tabs } from "antd";
import DeviceChart from "./components/DeviceChart/DeviceChart";
import ClassChart from "./components/ClassChart/ClassChart";
interface ChartManageProps {}

const ChartManage = (props: ChartManageProps) => {
  return (
    <Tabs
      defaultActiveKey="1"
      // onChange={onChange}
      items={[
        {
          label: `Biểu đồ lớp học`,
          key: "1",
          children: <ClassChart />,
        },
        {
          label: `Biểu đồ thiết bị`,
          key: "2",
          children: <DeviceChart />,
        },
      ]}
    />
  );
};

export default ChartManage;
