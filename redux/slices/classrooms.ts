import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { CLASS_ROOM } from "../../models/data";
export interface classRoomState {
  classRoom: CLASS_ROOM[];
}
const initialState: classRoomState = {
  classRoom: [],
};

export const classUserSlice = createSlice({
  name: "classroom",
  initialState,
  reducers: {
    addClassRoom: (state, { payload }) => {
      state.classRoom.push(payload);
    },
    removeClassRoom: (state, { payload }) => {
      const cloneArr = [...state.classRoom];

      const index = cloneArr.findIndex((item) => item.id === payload.id);
      cloneArr.splice(index, 1);
      state.classRoom = cloneArr;
    },
  },
});

// Action creators are generated for each case reducer function
export const { addClassRoom, removeClassRoom } = classUserSlice.actions;

export default classUserSlice.reducer;
