import React from "react";
import MainLayout from "../components/layout/MainLayout.component";
import { MENU_TYPE } from "../models/menu";
import { PicRightOutlined } from "@ant-design/icons";
import AdminPage from "../components/Schedule/Schedule.component";
import Link from "next/link";
import AdminLayout from "../components/layout/AdminLayout.component";
import { NextPageWithLayout } from "../models/common";
type Props = {};

const Admin: NextPageWithLayout = (props: Props) => {
  return <AdminPage />;
};
Admin.Layout = AdminLayout;
export default Admin;
