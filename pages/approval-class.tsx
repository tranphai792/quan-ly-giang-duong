import React from "react";
import ApprovalClass from "../components/ApprovalClass/ApprovalClass";
import AdminLayout from "../components/layout/AdminLayout.component";
import { NextPageWithLayout } from "../models/common";

const ApprovalClassPage: NextPageWithLayout = () => {
  return <ApprovalClass />;
};

export default ApprovalClassPage;

ApprovalClassPage.Layout = AdminLayout;
