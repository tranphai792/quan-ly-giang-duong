import React from "react";
import ClassRoomManage from "../components/ClassRoomManage/ClassRoomManage";
import AdminLayout from "../components/layout/AdminLayout.component";
import { NextPageWithLayout } from "../models/common";

const ClassManage: NextPageWithLayout = () => {
  return <ClassRoomManage />;
};

export default ClassManage;

ClassManage.Layout = AdminLayout;
