import React from "react";
import { LoginPage } from "../components/LoginPage/LoginPage.component";
import { NextPageWithLayout } from "../models/common";

const Login: NextPageWithLayout = () => {
  return <LoginPage />;
};

export default Login;
