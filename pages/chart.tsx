import React from "react";
import ChartManage from "../components/ChartManage/ChartManage";
import AdminLayout from "../components/layout/AdminLayout.component";
import { NextPageWithLayout } from "../models/common";

type Props = {};

const ChartPage: NextPageWithLayout = (props: Props) => {
  return <ChartManage />;
};
ChartPage.Layout = AdminLayout;
export default ChartPage;
