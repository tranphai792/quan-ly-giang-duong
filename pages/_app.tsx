import { Provider } from "react-redux";
import EmptyLayout from "../components/layout/EmptyLayout.component";
import { AppPropsWithLayout } from "../models/common";
import { store } from "../redux/store";
import "../styles/globals.scss";

export default function App({ Component, pageProps }: AppPropsWithLayout) {
  const Layout = Component.Layout ?? EmptyLayout;
  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />;
      </Layout>
    </Provider>
  );
}
