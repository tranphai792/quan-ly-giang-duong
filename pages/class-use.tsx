import * as React from "react";
import ClassRoomUsed from "../components/ClassUsed/ClassUsed.component";
import AdminLayout from "../components/layout/AdminLayout.component";
import { NextPageWithLayout } from "../models/common";

const ClassUsed: NextPageWithLayout = () => {
  return <ClassRoomUsed />;
};
ClassUsed.Layout = AdminLayout;

export default ClassUsed;
