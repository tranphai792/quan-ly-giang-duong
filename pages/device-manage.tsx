import React from "react";
import DeviceManage from "../components/DeviceManage/DeviceManage";
import AdminLayout from "../components/layout/AdminLayout.component";
import { NextPageWithLayout } from "../models/common";

type Props = {};

const Device_manage: NextPageWithLayout = (props: Props) => {
  return <DeviceManage />;
};
Device_manage.Layout = AdminLayout;
export default Device_manage;
