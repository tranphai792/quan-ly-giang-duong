import ClassRoomPage from "../components/ClassRoom/ClassRoom.component";
import AdminLayout from "../components/layout/AdminLayout.component";
import { NextPageWithLayout } from "../models/common";

export interface ClassProps {}

const Class: NextPageWithLayout = (props: ClassProps) => {
  return <ClassRoomPage />;
};
Class.Layout = AdminLayout;
export default Class;
